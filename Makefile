test_all: test3 test2 testjs clean

test2: clean sample
	@python2 -m pytest py_lib -vv

test3: clean sample
	@python3 -m pytest py_lib -vv

testjs: clean sample
	npm test

clean:
	@rm -rf py_lib/*.pyc py_lib/__pycache__ py_lib/*/*.pyc build .pytest_cache \
	py_lib/*/__pycache__ py_lib/*/*/*.pyc py_lib/*/*/__pycache__ build dist \
	predecessor.egg-info .pytest_cache README.rst

tarpy: clean rst
	@python3 setup.py sdist --formats=gztar

wheel: tarpy
	@python3 setup.py bdist_wheel --universal

sample:
	@mkdir -p ./build
	@touch ./build/test_case.object
	@python3 -c "from py_lib.tests import make_sample; make_sample()"

purge: clean
	@rm -rf node_modules

rst:
	@pandoc README.md --from=markdown --to=rst -o README.rst

publish_py: test2 test3 wheel
	@python3 -m twine upload ./dist/* -s -u gappleto97

publish_js: clean
	@npm publish .

publish: publish_py publish_js
