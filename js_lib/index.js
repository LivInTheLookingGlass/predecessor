const aggregation = exports.aggregation = require("aggregation/es6");
const msgpack = require('msgpack-lite');

function pack(obj) {
    let buff = msgpack.encode(obj);
    return Buffer.concat([msgpack.encode(buff.length), buff]);
}

function unpack(buff, header) {
    if (header) {
        [x, buff] = strip_length(buff);
    }
    return msgpack.decode(buff);
}

const strip_length = exports.strip_length = function strip_length(buff) {
    if (buff[0] < 0xf0) {
        return [msgpack.decode(buff.slice(0, 1)), buff.slice(1)];
    } else if (buff[0] == 0xcc || buff[0] == 0xd0) {
        return [msgpack.decode(buff.slice(0, 2)), buff.slice(2)];
    } else if (buff[0] == 0xcd || buff[0] == 0xd1) {
        return [msgpack.decode(buff.slice(0, 3)), buff.slice(3)];
    } else if (buff[0] == 0xce || buff[0] == 0xd2) {
        return [msgpack.decode(buff.slice(0, 5)), buff.slice(5)];
    } else if (buff[0] == 0xcf || buff[0] == 0xd3) {
        return [msgpack.decode(buff.slice(0, 9)), buff.slice(9)];
    }
}

const Serializable = exports.Serializable = class Serializable {
    constructor()   {
        this._slots = [];
    }

    serialized(...args) {
        let name = this.constructor.name;
        if (!args.length) {
            if (!this._slots.length)    {
                throw new Error("implied serialization is not implemented");
            } else {
                console.warn("No args given. Attempting to use _slots. This will not work if ordered improperly.");
                for (let x of this._args)   {
                    args.push(this[x]);
                }
            }
        }
        return pack([name, ...args]);
    }

    static deserialize(buff, header = true) {
        let [name, ...args] = unpack(buff, header);
        if (name !== this.prototype.constructor.name) {
            throw new Error("This is not a serialized instance of the right class");
        }
        return this.recombine(...args);
    }

    static recombine(...args) {
        return new this(...args);
    }
};

const SignedSerializable = exports.SignedSerializable = class SignedSerializable extends Serializable {
    constructor() {
        throw new Error("Not implemented");
    }
};

const EncryptedSerializable = exports.EncryptedSerializable = class EncryptedSerializable extends Serializable {
    constructor() {
        throw new Error("Not implemented");
    }
};

const SignedEncryptedSerializable = exports.SignedEncryptedSerializable = class SignedEncryptedSerializable extends aggregation(SignedSerializable, EncryptedSerializable) {
    constructor() {
        throw new Error("Not implemented");
    }
};

const _singleton_instances = new Map();

const Singleton = exports.Singleton = class Singleton {
    /**
     * This class can be inherited from whenever you want to create a singleton
     * class. Note that this __will__ break if you have two singleton classes of
     * the same name.
     **/
    constructor() {
        if (!_singleton_instances.has(this.constructor)) {
            _singleton_instances.set(this.constructor, this);
        }
        return _singleton_instances.get(this.constructor);
    }
};
