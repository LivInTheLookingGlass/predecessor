const expect = require('expect');
const Singleton = require('../index.js').Singleton;
const aggregation = require('../index.js').aggregation;

describe('The Singleton base class:', () => {
    it('always have a single instance', () => {
        let a = new Singleton();
        let b = new Singleton();
        a.test = true;
        expect(a).toBe(b);
        expect(b.test).toEqual(true);
    });

    it('should make different classes be different instances', () => {
        class Test extends Singleton {};
        let a = new Singleton();
        let b = new Test();
        a.test = true;
        expect(b.test).toEqual(undefined);
    });

    it('should be able to inherit from multiple classes (part 1)', () => {
        class Test1 {};
        class Test2 extends aggregation(Singleton, Test1) {};
        let a = new Singleton();
        let b = new Test2();
        a.test = true;
        expect(b.test).toEqual(undefined);
    });

    it('should be able to inherit from multiple classes (part 2)', () => {
        class Test1 extends Singleton {};
        class Test2 extends Test1 {};
        let a = new Singleton();
        let b = new Test2();
        a.test = true;
        expect(b.test).toEqual(undefined);
    });
});
