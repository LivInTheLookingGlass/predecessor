class SingletonMetaClass(type):
    """This metaclass is used from whenever you want to have only a single
    instance of a class. It is implemented using a metaclass, so the python2
    and python3 uses are different. Stubs are provided for both, so you do not
    need to deal with this metaclass directly, and it is not recommended to do
    so."""
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMetaClass, cls).__call__(
                *args, **kwargs)
        return cls._instances[cls]
