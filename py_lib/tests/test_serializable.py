from .. import Serializable
from . import Sample

import pytest

from collections import namedtuple


def test_single_instance():
    """This test ensures that Serializable classes will properly deserialize"""

    class Test(Serializable):
        def __init__(self, a):
            self.a = a

        def __eq__(self, o):
            return self.a == o.a

        def serialized(self):
            return super(Test, self).serialized(self.a)

    a = Test(3)
    b = Test.deserialize(a.serialized())
    assert a == b


def test_implied_serialization():
    """This test ensures that Serializable classes will properly deserialize"""

    class Test(Serializable):
        __slots__ = ('b', 'a', 'd')

        def __init__(self, b, a, d):
            self.a = a
            self.b = b
            self.d = d

    a = Test(1, 2, 3)
    a.serialized()


def test_undefined_serialization1():
    """This test ensures that Serializable classes will properly deserialize"""

    class Test(Serializable):
        def __init__(self, b, a, d):
            self.a = a
            self.b = b
            self.d = d

    class Test2(Test):
        def __init__(self, c):
            super(Test2, self).__init__(1, 2, 3)
            self.c = c

    a = Test2(3)
    with pytest.raises(NotImplementedError):
        a.serialized()


def test_undefined_serialization2():
    """This test ensures that Serializable classes will properly deserialize"""

    class Test(Serializable):
        pass

    a = Test()
    with pytest.raises(NotImplementedError):
        a.serialized()


def test_undefined_serialization3():
    """This test ensures that Serializable classes will properly deserialize"""

    class Test(Serializable, namedtuple("Test", "a b c")):
        pass

    a = Test(1, 2, 3)
    with pytest.raises(NotImplementedError):
        a.serialized()


def test_sample_deserialization():
    """This test ensures accurate deserialization of predefined test cases"""
    a = None
    with open('./build/test_case.object', 'rb') as f:
        a = Sample.deserialize(f.read())
    assert a is not None
