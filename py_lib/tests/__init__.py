from .. import Serializable


def make_sample():
    with open('./build/test_case.object', 'wb') as f:
        f.write(Sample(1, 2).serialized())


class Sample(Serializable):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def serialized(self):
        return super(Sample, self).serialized(self.a, self.b)
