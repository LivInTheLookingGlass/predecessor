from .. import Singleton


def test_single_instance():
    """This test ensures that Singletons always have a single instance"""

    class Test(Singleton):
        def __init__(self, a):
            self.a = a

    assert Test(1) is Test(2)


def test_multiple_subclasses():
    """This test ensures that Singletons of different classes are not the same
    instance"""

    class TestA(Singleton):
        def __init__(self, a):
            self.a = a

    class TestB(Singleton):
        def __init__(self, a):
            self.b = a

    assert TestA(1) is not TestB(2)


def test_multiple_inheritance():
    """This test ensures that Singletons can inherit from other classes as
    well"""

    class Test(int, Singleton):
        pass

    assert Test(1) is Test(2)
